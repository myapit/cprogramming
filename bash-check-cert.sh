#!/bin/bash

echo -e "\e[100mWelcome to CLI Cert Display\e[0m"
echo "==========================="
for (( ; ; ))
do
        read -p "Enter URL WITHOUT https:// >: " URL
        curl --insecure -v https://$URL 2>&1 | awk 'BEGIN { cert=0 } /^\* SSL connection/ { cert=1 } /^\*/ { if (cert) print }'
        echo -e "\e[31m[ Hit CTRL + C to exit program ]\e[0m"
        echo ""
done
